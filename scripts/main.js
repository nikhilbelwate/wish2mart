class Product{
    constructor(id, name, url, price, details, category){
        this.id=id;
        this.name=name;
        this.url=url;
        this.price=price;
        this.details=details;
        this.category=category;
    }
    getTax(){
        return this.price*0.12;
    }
    toString(){
        return (`<div class="card">
        <a href="./product_detail.html?pid=${this.id}">
        <img class="card-img-top" src="${this.url}" alt="Card image cap"></a>
        <div class="card-block"><a href="./product_detail.html?pid=${this.id}">
          <h4 class="card-title">${this.name}</h4>
          <h5 class="card-category">${this.category}</h5>
          <p class="card-text">Price: <strong>$ ${this.price}</strong></p></a>
          </div></div>`);
    }
}

const products=Array();
products.push(new Product(0,"sony earphone","https://cdn.shopify.com/s/files/1/0057/8938/4802/products/5ea15cf3-7454-4851-9342-cd77f8af418f_600x.png",25.75,"It is big jbjdb kjbjkdb nbdhb hdhgdhdbh dhagdhjgd hfdgshfvhs hdfgshfvh hgfhdgf","Earphone"));
products.push(new Product(1,"Boat Headphone","https://m.media-amazon.com/images/W/IMAGERENDERING_521856-T1/images/I/61ljxTBpTCL._SL1500_.jpg",30,
"Much Better Sound - Powerful speaker with the BT 5.0 technology. High quality built-in microphones software and HD sound technology, offer you high fidelity stereo music with deep bass and crystal clear speech when you listen to music or watch movies.Hand-free call & Built in Mic - Provide a quick and stable connection with your BT enabled devices like cell phones, tablets, PC, TVs within 33 feet, with a high-quality built-in microphone for hands-free calls, which is convenient for you to free yourself from wires.     Both Wireless and Wired Mode - BT headphones can work well both in wire and wireless mode. Battery can be used 20-hours music time/ talking time in a single charge for wireless mode. ",
"Headphone"));
products.push(new Product(2,"Denim shirt","https://m.media-amazon.com/images/W/IMAGERENDERING_521856-T2/images/I/A1wEjvjUA-L._AC_UX679_.jpg",
75.33,"<ul><li> 100% Cotton </li>  <li> Button closure </li>  <li>Machine Wash  </li>  <li> Left chest pocket with button closure and pencil slot </li>  <li> Double button adjustable cuffs </li>  <li> Straight back yoke with center box pleat </li>  <li> Garment washed for softness </li>  <li> Stonewash </li>  </ul>",
"cloths"));
products.push(new Product(3,"Nike Dunks","https://di2ponv0v5otw.cloudfront.net/posts/2023/02/14/63ebd977ffb5d08869b92311/m_wp_63ebd9968bb2e29a45d9ca20.webp",
350.00,"<ul><li> Nike shoes </li>  <li> color:Green/White </li>  <li>Size 6  </li>  <li>Michigan state dunks </li> </ul>",
"Shoes"));
products.push(new Product(4,"varsity jackets","https://www.jacketscreator.com/wp-content/uploads/2020/04/brown-varsity-jacket.jpg",
75.99,"<ul><li> 100% Leather </li>  <li> Button closure </li>  <li>Mens Brown Varsity jacket with creame leather sleeves </li>  </ul>",
"cloths"));
products.push(new Product(5,"Apple Pen","https://m.media-amazon.com/images/I/51uuTM0tI-L._AC_SL1500_.jpg",
599.99,"<ul><li> TiMOVO Stylus Pen for iPad </li>  <li> Palm Rejection </li>  <li>Tilt High Precision Apple Pencil Compatible with（2018-2022） iPad Pro 2021 11/12.9/iPad Air 5th 4th 3rd,iPad... </li>  </ul>",
"Gadgets"));

products.push(new Product(6,"Apple Watch","https://m.media-amazon.com/images/I/71QGyw3tDBL._AC_SL1500_.jpg",
499.99,"<ul><li> Apple Watch Series 8</li> <li> [GPS 45mm] Smart Watch w/Midnight Aluminium Case with Midnight Sport Band.</li> <li>Fitness Tracker, Blood Oxygen & ECG Apps, Always-On...</li> </ul>",
"Gadgets"));
products.push(new Product(7,"Scooter","https://vintageironcyclesvancouver.s3.us-west-2.amazonaws.com/wp-content/uploads/2022/08/31123413/DSC09791-1.jpg",
3499.99,"<ul><li>NEW! 2023 Synergy Cyclone </li> <li> Dual 1000W Electric Scooter </li> </ul>",
"Gadgets"));
products.push(new Product(8,"Apple Laptop","https://m.media-amazon.com/images/I/71LfhpWLA5L._AC_SL1500_.jpg",
1499.99,"<ul><li> Apple 2022 MacBook Air Laptop </li>  <li> M2 chip: 13.6-inch Liquid Retina Display, 8GB RAM, 256GB SSD Storage, Backlit Keyboard, 1080p FaceTime HD Camera. Works... </li></ul>",
"Gadgets"));
products.push(new Product(9," adidas yeezy slides","https://www.buyyzys.org/media/image-description/GY0807%20(3).jpg",
299.99,"<ul><li> Yeezy slides </li>  <li>Glow Green </li>  <li>Water Proof</li>  </ul>",
"Footwear"));
products.push(new Product(10,"Apple iphone 11","https://m.media-amazon.com/images/I/81ldhum0M4L._AC_SL1500_.jpg",
641.21,"<ul><li> Apple iPhone 11 Pro </li>  <li>US Version, 256GB  </li>  <li>Space Gray </li>  <li> Unlocked (Renewed) </li> </ul>",
"Gadgets"));
products.push(new Product(11,"Wallet","https://m.media-amazon.com/images/I/81+dO5qLBtL._AC_SL1500_.jpg",
32.95,"<ul><li> 100% Cotton </li> RUNBOX Slim Wallets for Men <li> RFID Blocking & Minimalist Mens Front Pocket Wallet Leather… </li>  <li>Carbon black </li> </ul>",
"accessory"));
products.push(new Product(12,"Mens Bracelets","https://m.media-amazon.com/images/I/61GE1x2XceL._AC_UL1001_.jpg",
13.99,"<ul><li> Mixed Wrap Leather Wristbands </li>  <li> Bracelets and Wood Beads Bracelet Set for Men Women </li>  <li>Adjustable 4 Pieces Beads Braided Leather Believe Charm Bracelet for Men  </li>  </ul>",
"Accessory"));
products.push(new Product(13,"Eye Frames For Men","https://cdn.vooglam.com/media/catalog/product/0/1/01_192.jpg",
39.99,"<ul><li> Frames For Men </li>  <li> Aviator Gold </li> <li>Prescription Glasses </li> </ul>",
"Accessory"));
products.push(new Product(14,"Custom watch for women","https://m.media-amazon.com/images/I/51g6fuO0M5L._AC_.jpg",
19.99,"<ul><li> Womens watch </li>  <li> Lancardo Rhinestone Gold-Tone Luxury </li> <li>Stainless Steel Quartz Unisex Wrist Watch for  Women </li> </ul>",
"Accessory"))

products.push(new Product(15,"School Bag","https://m.media-amazon.com/images/I/514eAToOMtL._AC_UL1001_.jpg",
89.99,"<ul><li> 17inch Rolling Backpack,Large Backpack with Wheels for Men Women Adults,Waterproof Wheeled Travel Laptop Backpack,Carry on Luggage Bag Trolley Suitcase Business College School Computer Bookbag,Black17inch Rolling Backpack,Large Backpack with Wheels for Men Women Adults,Waterproof Wheeled Travel Laptop Backpack,Carry on Luggage Bag Trolley </li>  <li>Black </li>   </ul>",
"Accessory"));
products.push(new Product(16,"Heeled Chelsea Boots","https://www.gapcanada.ca/webcontent/0050/776/206/cn50776206.jpg",
70.00,"<ul><li> Gap women Chelsea Boots </li>  <li> Earth Brown </li>   </ul>",
"Footwear"));
products.push(new Product(17,"Swimming Googles","https://m.media-amazon.com/images/I/71pGKAx7c3L._AC_UL1500_.jpg",
19.99,"<ul><li> Swimming Goggles </li>   <li> Anti Fog Swim Goggles with UV </li>  <li> Unisex for Adult and Kids </li>  </ul>",
"Eye wear"));
products.push(new Product(18,"Fitted hats for men","https://i5.walmartimages.com/asr/904f5505-9905-432f-92ba-463beedd5278.e2d1e8fe668c2221c51bc69cc5e4e6fb.jpeg",
34.99,"<ul><li>Newyork fitted hat</li><li>Sky/Dark Blue</li></ul>",
"clothes"));
products.push(new Product(19,"Leather jackets for men","https://m.media-amazon.com/images/I/81E4XopJD7L._AC_UL1500_.jpg",
99.99,"<ul><li> Wantdo Men's Leather Jacket with Removable Hood</li><li>Stone Black</li></ul>",
"clothes"));
products.push(new Product(20,"Belt For Men","https://th.bing.com/th/id/R.e5d00ae96b290be954cda657817037ed?rik=6Hzb9kkb3KvVCA&pid=ImgRaw&r=0",
69.99,"<ul><li> 100% Leather belt </li>  <li> brown </li> </ul>",
"clothes"));
products.push(new Product(21,"Mens shoes","https://m.media-amazon.com/images/I/612IxfDQjfL._AC_UL1500_.jpg",
34.99,"<ul><li> 100% Leather shoe </li>  <li> black </li> </ul>",
"Foot wear"));
products.push(new Product(22,"Mens slippers","https://m.media-amazon.com/images/I/714WXXwIGSL._AC_UL1500_.jpg",
54.99,"<ul><li> OluKai Ohana Men's Beach Sandals </li>  <li>Quick-Dry Flip-Flop Slides, Water Resistant & Lightweight, Compression Molded Footbed & Ultra-Soft Comfort Fit </li> </ul>",
"Beauty"));
products.push(new Product(23,"Mens hair clipper","https://m.media-amazon.com/images/I/812G7-ZOtES._AC_SX679_.jpg",
44.99,"<ul><li> WAHL Canada Elite Pro High Performance Home Hair Cutting Kit, At home Haircutting, Electric Hair Clipper, Grooming Kit for Men, Electric Hair Clipper, Certified in Canada, Model 3145 </li>  <li> Black </li> </ul>",
"clothes"));
products.push(new Product(24," Appple i pad","https://m.media-amazon.com/images/I/51dBcW+NXPL._AC_SL1000_.jpg",
223.99,"<ul><li>Apple iPad 9.7</li>  <li> space grey </li><li>2017 model with 32gb</li> </ul>",
"Gadgets"));

products.push(new Product(25,"Play station 5","https://m.media-amazon.com/images/I/615rF4XbrbL._AC_SX679_.jpg",
699.99,"<ul><li> PlayStation 5 Console - Disc Edition - God of War Ragnarök Bundle </li>  <li>White </li> </ul>",
"Gadgets"));
products.push(new Product(26,"BlueTooth speakers","https://m.media-amazon.com/images/I/61FxqzQX0TL.__AC_SY300_SX300_QL70_ML2_.jpg",
89.95,"<ul><li> BOSS Audio Systems Tube Portable Weatherproof Bluetooth Speaker - 2 3-Inch Speakers, 8 Hours of Play Time, Sold Individually </li>  <li> Black/Blue </li> </ul>",
"Gadgets"));
products.push(new Product(27,"Stockings for men","https://fanatics.frgimages.com/atlanta-braves/mens-stance-atlanta-braves-stripe-diamond-pro-over-the-calf-socks_pi3504000_ff_3504060-8a2a05d1118dba949739_full.jpg?_hv=2&w=900",
89.95,"<ul><li> 100% cotton Men's Atlanta Braves Stance Stripe Diamond Pro Over the Calf Socks </li>  <li> White/Blue/Red</li> </ul>",
"clothes"));
products.push(new Product(28,"Earings for women","https://cdn-fsly.yottaa.net/609426734f1bbfff95ac5591/ca.pandora.net/v~4b.1a/dw/image/v2/AAVX_PRD/on/demandware.static/-/Sites-pandora-master-catalog/default/dw4e1c1476/productimages/singlepackshot/292412C01_RGB.jpg?sw=900&sh=900&sm=fit&sfrm=png&bgcolor=F5F5F5&yocs=1_",
69.99,"<ul><li> Pandora Brilliance Lab-created 0.20 ct tw Diamond Hoop Earrings</li>  <li> Diamond </li> </ul>",
"Accessory"));
products.push(new Product(29,"Women gown","https://litb-cgis.rightinthebox.com/images/640x640/202210/bps/product/inc/edybmg1666676671899.jpg",
149.99,"<ul><li>Women‘s Swing Dress Vintage Dress Midi Dress Red Long Sleeve Pure Color Ruched Winter Fall Autumn V Neck 2023 Style S M L XL 2XL  </li>  <li> Red </li> </ul>",
"clothes"));
products.push(new Product(30,"Suit Men","https://m.media-amazon.com/images/I/61zHtPUfIqL._AC_UL1200_.jpg",
119.99,"<ul><li> MAGE MALE Men's 3 Pieces Suit Elegant Solid One Button Slim Fit Single Breasted Party Blazer Vest Pants Set </li>  <li> Black </li> </ul>",
"clothes"));

 