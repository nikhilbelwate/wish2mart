var cart_qty=1;
// Single Item Constructor
function Item(id,name, price) {
    this.id=id;
    this.name = name;
    this.price = price;
    this.QTY=1;
  }

 function addItemToCart(item){
    //alert(sessionStorage.getItem("cart"));
    let cart=JSON.parse(sessionStorage.getItem("cart"));

    if(cart==null){
        cart=new Map();
    }else{
        cart=new Map(cart);
    }
    if(cart.get(item.id)){
        i=cart.get(item.id);
        i.QTY++;
        cart.set(i.id,i);
    }else{    
        cart.set(item.id,new Item(item.id,item.name,item.price));
    }
    
    // Convert Map to string
    const mapString = JSON.stringify([...cart]);
    
    // Store the string in sessionStorage
    sessionStorage.setItem('cart', mapString);
    sessionStorage.getItem("cart_qty")==undefined?sessionStorage.setItem("cart_qty",cart_qty):sessionStorage.setItem("cart_qty",parseInt(sessionStorage.getItem("cart_qty"))+1);
    document.getElementById("cart_count").innerHTML=sessionStorage.getItem("cart_qty");
    if(document.getElementById("view_cart_items")!=null){
        displayCart();
    }
}
 function removeItemToCart(id, qt=1){
    let cart=JSON.parse(sessionStorage.getItem("cart"));

    if(cart==null){
        cart=new Map();
    }else{
        cart=new Map(cart);
    }
    
    if(cart.has(id)){
        itm=cart.get(id);
        itm.QTY=itm.QTY-qt;
        if(itm.QTY<=0){
            cart.delete(id); 
        }else{
            cart.set(id,itm);
        }
    }
    // Convert Map to string
    const mapString = JSON.stringify([...cart]);
    
    // Store the string in sessionStorage
    sessionStorage.setItem('cart', mapString);
    sessionStorage.getItem("cart_qty")==undefined?sessionStorage.setItem("cart_qty",0):sessionStorage.setItem("cart_qty",parseInt(sessionStorage.getItem("cart_qty"))-qt);
    document.getElementById("cart_count").innerHTML=sessionStorage.getItem("cart_qty");
    if(document.getElementById("view_cart_items")!=null){
        displayCart();
    }
 }
 function displayCart(){
    item_view=document.getElementById("view_cart_items");
    item_view.innerHTML="";
    price_view=document.getElementById("price_view");
    price_view.innerHTML="";
    let cart=JSON.parse(sessionStorage.getItem("cart"));
    if(cart==null){
        cart=new Map();
    }else{
        cart=new Map(cart);
    }
    let items =cart.values();
    let total =0;
    let tax=0;
    // Iterate over the iterator and get the values using a for...of loop
    for (const i of items) {
        total=total+ i.price * i.QTY;
        tax=tax + (i.price * i.QTY) *0.12;
        price_view.innerHTML=`<li id="rec_${i.id}" class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-2">
        <span>${i.name}</span>
        <span>X ${i.QTY}</span>
        <span>$ ${i.price * i.QTY}</span>
        </li>`+price_view.innerHTML;
        item_view.innerHTML=`<div class="row mb-4">
        <div class="col-md-5 col-lg-3 col-xl-3">
            <div class="rounded mb-3 mb-md-0">
                <img class="img-fluid w-100 rounded" src=${products[i.id].url} alt="Sample">
            </div>
        </div>
        <div class="col-md-7 col-lg-9 col-xl-9">
            <div>
                <div class="d-flex justify-content-between">
                    <div>
                        <h5>${products[i.id].name}</h5>
                        <p class="mb-2 text-muted text-uppercase small">Category: ${products[i.id].category}</p>
                        <p class="mb-3 text-muted text-uppercase small"></p>
                    </div>
                    <div>
                        <div class="mb-0 w-100">
                            <button
                                onclick="this.parentNode.querySelector('input[type=number]').stepDown(); removeItemToCart(${i.id})"
                                class="btn btn-sm btn-outline-primary">
                                <i class="fas fa-minus"></i>
                            </button>
                            <input class="quantity text-center" disabled min="0" max="10" name="quantity"
                                value=${i.QTY} type="number">
                            <button
                                onclick="this.parentNode.querySelector('input[type=number]').stepUp(); addItemToCart(products[${i.id}]);"
                                class="btn btn-sm btn-outline-primary">
                                <i class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        
                    </div>
                    <p class="mb-0"><span><strong id="summary">$ ${i.price}</strong></span></p
                        class="mb-0">
                </div>
            </div>
        </div>
    </div>
    <hr class="mb-4">`+item_view.innerHTML;
    }
    tax=tax.toFixed(2);
    totalAfterTax=total+tax;
    price_view.innerHTML=price_view.innerHTML+`<li
    class="list-group-item d-flex justify-content-between align-items-center border-top px-0 mb-3">
    <div>
        <strong>Total</strong>
    </div>
    <span><strong id=>$ ${total}</strong></span>
</li>  
<li
    class="list-group-item d-flex justify-content-between align-items-center border-top px-0 mb-3">
    <div>
        <strong>Tax (12%)</strong>
        
    </div>
    <span><strong>$ ${tax}</strong></span>
</li>
<li
    class="list-group-item d-flex justify-content-between align-items-center border-top px-0 mb-3">
    <div>
        <strong>Total Amount</strong>
        <strong>
            <p class="mb-0">(including TAX)</p>
        </strong>
    </div>
    <span><h4><strong>$ ${totalAfterTax}</strong></h4></span>
</li>`;
    
 }